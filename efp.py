# Standard imports 
import numpy as np
import pickle
import os
import pandas as pd 
import matplotlib.pyplot as plt
import json
from pathlib import Path
import xgboost as xgb
# GPAW imports
from gpaw import GPAW,PW, FermiDirac
from gpaw.utilities.ps2ae import PS2AE
from gpaw.mpi import serial_comm
from gpaw.response.tool import get_bz_transitions, get_chi0_integrand
from gpaw.response.pair import PairDensity
from gpaw.utilities import unpack
from gpaw.utilities.dos import raw_orbital_LDOS
import gpaw.mpi as mpi
# ASE imports
from ase.atoms import Atoms
from ase.io import read,write
from ase.phonons import Phonons
from ase.units import Ha, Bohr, Hartree
from ase.parallel import world, parprint, paropen,MPI4PY
from ase.neighborlist import neighbor_list
from ase.utils.timing import Timer
from asr.core import read_json,file_barrier
from asr.utils.kpts import get_kpts_size

import torch

from utils import get_domain_decomp
from NN import Net

class MLGPAW:
    def __init__(self,calc,calc_name=None,model_path=None,converge_wave_functions=True,calculate_matrix_elements=True):
        self.timer = Timer()
        if isinstance(calc,str):
            self.calc_name = calc
            self.calc = GPAW(calc)
        elif isinstance(calc,GPAW):
            assert calc_name != None, 'Please provide name for calculator using calc_name'
            self.calc_name = calc_name
            self.calc = calc
        
        self.path = '/'.join(self.calc_name.split('/')[:-1])+'/' if '/' in self.calc_name else './'
        if model_path is None:
            self.model_path = 'xgb_model_all_alphas.json'
        else:
            self.model_path = model_path
        self.fp_df = None
        
        self.nbands = self.calc.get_number_of_bands()
        self.nkpts = len(self.calc.get_ibz_k_points())
        self.natoms = self.calc.atoms.get_global_number_of_atoms()
        
        parprint('GPAW calculator {} loaded on {} core(s)'.format(self.calc_name,self.calc.world.size))
        parprint('Number of bands = {}, number of k-points = {}'.format(self.nbands,self.nkpts))
        parprint('world.size = {}, calc.world.size = {}'.format(world.size,self.calc.world.size))
       
        
        self.matrix_elements_file = self.calc_name.split('.gpw')[0] + '_matrix_elements.npz'
        self.matrix_elements_calculated = os.path.isfile(self.matrix_elements_file)
        self.nabla_knmr = None
        self.p2_knn = None

        self.wfs_converged = not isinstance(self.calc.wfs.kpt_u[0].psit_nG,type(None))
        if self.calc_name.split('.gpw')[0][-2:] == 'ml':
            self.eigenvalues_updated = True
            self.calc_name = ''.join(self.calc_name.split('_ml'))
        else:
            self.eigenvalues_updated = False
        
        if not self.eigenvalues_updated:
            if not self.matrix_elements_calculated:
                parprint('Matrix elements not calculated')
                if not self.wfs_converged and converge_wave_functions:
                    parprint('Wavefunctions not converged.')
                    parprint('Converging wavefunctions')
                    self.timer.start('Converging wavefunctions')
                    self.calc.converge_wave_functions()
                    parprint('Number of bands = {}, number of k-points = {}'.format(self.calc.get_number_of_bands(),len(self.calc.get_ibz_k_points())))
                    self.timer.stop('Converging wavefunctions')
                    self.wfs_converged = True
                        
                else:
                    parprint('Wavefunctions already converged.')
                if calculate_matrix_elements and self.wfs_converged:
                    parprint('Writing GPAW object with wfs.')
                    self.calc.write(self.calc_name.split('.gpw')[0]+'_wfs.gpw',mode='all')
                    if not os.path.isfile(self.calc_name):
                        self.calc.write(self.calc_name)
                    parprint('Calculating matrix elements')
                    self.timer.start('Calculating matrix elements')
                    parprint('Reading GPAW wfs in serial')
                    self.calc = GPAW(self.calc_name.split('.gpw')[0]+'_wfs.gpw',communicator=mpi.serial_comm)
                    parprint('world.size = {}, calc.world.size = {}, calc.wfs.world.size = {}'.format(world.size,self.calc.world.size,self.calc.wfs.world.size))
                    if world.rank == 0:
                        os.remove(self.calc_name.split('.gpw')[0]+'_wfs.gpw')                
                    
                    nabla_knmr = self.get_nabla_matrix_elements()
                    self.nabla_knmr = nabla_knmr
                    p2_knn = self.get_p2_matrix_elements()
                    self.p2_knn = p2_knn
                    #if world.rank == 0:
                    parprint('Saving matrix elements to {}'.format(self.matrix_elements_file))
                    np.savez(self.matrix_elements_file, nabla_knmr=nabla_knmr, p2_knn=p2_knn)
                    self.matrix_elements_calculated = True

                    parprint('Reading GPAW in parallel')
                    self.calc = GPAW(self.calc_name)
                    parprint('world.size = {}, calc.world.size = {}'.format(world.size,self.calc.world.size))
                    self.timer.stop('Calculating matrix elements')
            else:
                parprint('Matrix elements already calculated')
                self.load_matrix_elements()
            
    def load_matrix_elements(self):
        #with file_barrier([self.matrix_elements_file]):
        matrix_elements = np.load(self.matrix_elements_file,allow_pickle=False)
        self.nabla_knmr = matrix_elements['nabla_knmr']
        self.p2_knn = matrix_elements['p2_knn']

    def get_nabla_matrix_elements(self,
             ecut = 0.1
             ):
        self.timer.start('Calculating nabla elements')
        pd = PairDensity(gs=self.calc,ecut=ecut,nblocks=1)#world.size)
        parprint('pd blockcomm.size = {}'.format(pd.blockcomm.size))
        
        E_f = self.calc.get_fermi_level()
        
        bzk_kc = self.calc.get_bz_k_points()
        bzmap = self.calc.get_bz_to_ibz_map()

        n1 = 0
        n2 = self.calc.get_number_of_bands()
        bands = np.arange(n1,n2)
        nkpts = self.nkpts
        # nkpts = len(bzk_kc)

        #parprint('Bands: {}'.format(bands))
        nabla_knmr = np.zeros((nkpts,len(bands),len(bands),3),dtype=complex)

        for k_idx in range(nkpts):
        # for k_idx in range(len(bzk_kc)):
            bz_k = np.where(bzmap==k_idx)
            bz_k = bz_k[0][0]
            # bz_k = k_idx
            kpt = pd.get_k_point(s=0, k_c=bz_k, n1=n1, n2=n2)
            for n_idx,n in enumerate(bands):
                n0_mv = pd.optical_pair_velocity(n=n,m_m=0,kpt1=kpt,kpt2=kpt,block=False)
                nabla_knmr[k_idx,n_idx,:,:] = n0_mv
        parprint('nabla_knmr: {}'.format(nabla_knmr.shape))
        self.timer.stop('Calculating nabla elements')
        return nabla_knmr
    
    def get_p2_matrix_elements(self) -> np.ndarray:
        """Calculate nabla squared matrix elements.

        Returns matrix of shape:

            (nibzkpts, nbands, nbands)

        in units of Hartree.
        """
        self.timer.start('Calculating p2 elements')
        assert self.calc.world.size == 1
        pd = self.calc.wfs.pd
        setups = self.calc.wfs.setups
        
        p2_knn = []
        for q, kpt_s in enumerate(self.calc.wfs.kpt_qs):
            #assert len(kpt_s) == 1
            kpt = kpt_s[0]
            psit = kpt.psit
            psit.read_from_file()
            projections = kpt.projections
            c_nG = psit.array
            p2_G = pd.G2_qG[q]
            p2_nn = pd.integrate(c_nG, c_nG * p2_G)
            for a, P_ni in projections.items():
                dp2_ii = 2 * unpack(setups[a].K_p)
                p2_nn += P_ni.conj() @ dp2_ii @ P_ni.T
            p2_knn.append(p2_nn)
        p2_knn = np.array(p2_knn)
        parprint('p2_knn: {}'.format(p2_knn.shape))
        self.timer.stop('Calculating p2 elements')
        return p2_knn


    def endome(self,E_E=np.linspace(-10, 10, 25), 
                    E_width=0.5,
                    E_alpha=0.0,
                    bands=None
                    ):
        self.timer.start('Calculating ENDOME material fingerprint')
        parprint('Calculating ENDOME fingerprints')

        if not self.matrix_elements_calculated:
            print('Matrix elements not calculated')
            return
        
        if self.nabla_knmr is None or self.p2_knn is None:
            self.load_matrix_elements()
        nabla_knmr = np.abs(self.nabla_knmr)
        p2_knn = np.abs(self.p2_knn)
        
        E_f = self.calc.get_fermi_level()

        bz_to_ibz_map = self.calc.get_bz_to_ibz_map()

        nkpts = self.nkpts
        if bands == None:
            nbands = self.calc.get_number_of_bands()
            lb,ub = 0,nbands
        else:
            lb,ub = bands
            nbands = ub-lb
        # eps_kn = np.zeros((nkpts,nbands))
        eps_kn = np.array([self.calc.get_eigenvalues(k) for k in range(nkpts)])
        # occ_kn = np.zeros((nkpts,nbands))
        # nabla_knm_inplane = np.zeros((nkpts,nbands,nbands))
        # nabla_knm_outplane = np.zeros((nkpts,nbands,nbands))

        nabla_knm_inplane = np.linalg.norm(nabla_knmr[:,:,:,:2],axis=3)
        nabla_knm_outplane = nabla_knmr[:,:,:,2]

        # for k_idx in range(nkpts):
        #     # eps_kn[k_idx,:] = self.calc.get_eigenvalues(k_idx)
        #     nabla_knm_inplane[k_idx,:,:] = np.linalg.norm(nabla_knmr[bz_to_ibz_map==k_idx,:,:,:2],axis=3).mean(axis=0)
        #     nabla_knm_outplane[k_idx,:,:] = nabla_knmr[bz_to_ibz_map==k_idx,:,:,2].mean(axis=0)


        eps_knm = eps_kn[:,np.newaxis,:]-eps_kn[:,:,np.newaxis]
        rec_eps_knm = eps_knm.copy()
        rec_eps_knm[eps_knm==0] = 1
        rec_eps_knm = 1/rec_eps_knm
        rec_eps_knm[eps_knm==0] = 0
        rec_eps_knm = np.abs(rec_eps_knm)

        K = self.nkpts
        M = nbands*len(E_E)
        start_index, end_index, local_k_size, numelements, displacements = get_domain_decomp(K, M, world.rank, world.size)
        kpoints = range(start_index,end_index)
        nkpoints = len(kpoints)
        
        gaussian_knmE = gaussian(E_E - eps_knm[:,:,:, np.newaxis], E_width) * np.exp(-E_alpha * np.abs(E_E))
        # gaussian_knE = gaussian(E_E - eps_kn[:,:, np.newaxis], E_width) * np.exp(-E_alpha * np.abs(E_E))
        gaussian_knmE *= np.where(E_f - eps_kn[:,np.newaxis,:,np.newaxis] >= 0,1,-1)
        #parprint('gaussian_knmE: {}'.format(gaussian_knmE.shape))

        gaussian_knmE = gaussian_knmE[kpoints,:,:,:]
        rec_eps_knm = rec_eps_knm[kpoints,:,:]
        nabla_knm_inplane = nabla_knm_inplane[kpoints,:,:]
        nabla_knm_outplane = nabla_knm_outplane[kpoints,:,:]
        p2_knn = p2_knn[kpoints,:,:]
        
        self.timer.start('np.einsum')
        dos_knE = np.einsum('lnm, knmE -> knE',np.ones(nabla_knm_inplane.shape)/nkpoints,gaussian_knmE,optimize=True)
        xy_knE = np.einsum('knm, knmE -> knE',nabla_knm_inplane*rec_eps_knm,gaussian_knmE,optimize=True)
        z_knE = np.einsum('knm, knmE -> knE',nabla_knm_outplane*rec_eps_knm,gaussian_knmE,optimize=True)
        nabla_inplane_knE = np.einsum('knm, knmE -> knE',nabla_knm_inplane,gaussian_knmE,optimize=True)
        nabla_outplane_knE = np.einsum('knm, knmE -> knE',nabla_knm_outplane,gaussian_knmE,optimize=True)
        p2_knE = np.einsum('knm, knmE -> knE',p2_knn,gaussian_knmE,optimize=True)
        self.timer.stop('np.einsum')

        if self.calc.world.size > 1:
            self.timer.start('Gathering fp from ranks')
            MPI = MPI4PY()
            MPI_COMM_WORLD = MPI.comm
            rank = MPI_COMM_WORLD.rank
            size = MPI_COMM_WORLD.size

            dos_knE = dos_knE.reshape((local_k_size,-1)).astype(np.double) if local_k_size > 0 else np.zeros((1,M))
            global_dos_knE = np.zeros((K,M), dtype = np.double)
            xy_knE = xy_knE.reshape((local_k_size,-1)).astype(np.double) if local_k_size > 0 else np.zeros((1,M))
            global_xy_knE = np.zeros((K,M), dtype = np.double)
            z_knE = z_knE.reshape((local_k_size,-1)).astype(np.double) if local_k_size > 0 else np.zeros((1,M))
            global_z_knE = np.zeros((K,M), dtype = np.double)
            nabla_inplane_knE = nabla_inplane_knE.reshape((local_k_size,-1)).astype(np.double) if local_k_size > 0 else np.zeros((1,M))
            global_nabla_inplane_knE = np.zeros((K,M), dtype = np.double)
            nabla_outplane_knE = nabla_outplane_knE.reshape((local_k_size,-1)).astype(np.double) if local_k_size > 0 else np.zeros((1,M))
            global_nabla_outplane_knE = np.zeros((K,M), dtype = np.double)
            p2_knE = p2_knE.reshape((local_k_size,-1)).astype(np.double) if local_k_size > 0 else np.zeros((1,M))
            global_p2_knE = np.zeros((K,M), dtype = np.double)

            MPI_COMM_WORLD.Barrier()
            MPI_COMM_WORLD.Allgatherv([dos_knE, local_k_size*M], [global_dos_knE, (numelements, displacements)])  
            MPI_COMM_WORLD.Allgatherv([xy_knE, local_k_size*M], [global_xy_knE, (numelements, displacements)])  
            MPI_COMM_WORLD.Allgatherv([z_knE, local_k_size*M], [global_z_knE, (numelements, displacements)])  
            MPI_COMM_WORLD.Allgatherv([nabla_inplane_knE, local_k_size*M], [global_nabla_inplane_knE, (numelements, displacements)])  
            MPI_COMM_WORLD.Allgatherv([nabla_outplane_knE, local_k_size*M], [global_nabla_outplane_knE, (numelements, displacements)])  
            MPI_COMM_WORLD.Allgatherv([p2_knE, local_k_size*M], [global_p2_knE, (numelements, displacements)])  

            dos_knE = global_dos_knE.reshape((self.nkpts,self.nbands,len(E_E)))
            xy_knE = global_xy_knE.reshape((self.nkpts,self.nbands,len(E_E)))
            z_knE = global_z_knE.reshape((self.nkpts,self.nbands,len(E_E)))
            nabla_inplane_knE = global_nabla_inplane_knE.reshape((self.nkpts,self.nbands,len(E_E)))
            nabla_outplane_knE = global_nabla_outplane_knE.reshape((self.nkpts,self.nbands,len(E_E)))
            p2_knE = global_p2_knE.reshape((self.nkpts,self.nbands,len(E_E)))

            self.timer.stop('Gathering fp from ranks')

        parprint('Done')
        self.timer.stop('Calculating ENDOME material fingerprint')
        return {'dos_knE':dos_knE[:,lb:ub,:],
                'xy_knE':xy_knE[:,lb:ub,:],
                'z_knE':z_knE[:,lb:ub,:],
                'nabla_inplane_knE':nabla_inplane_knE[:,lb:ub,:],
                'nabla_outplane_knE':nabla_outplane_knE[:,lb:ub,:],
                'p2_knE':p2_knE[:,lb:ub,:]}
    
    def endome_material(self,E_E=np.linspace(-10, 10, 25), 
                        E_width=0.5,
                        E_alpha=0.0,
                        bands=None
                        ):
        self.timer.start('Calculating ENDOME fingerprint')
        parprint('Calculating ENDOME material fingerprints')

        if not self.matrix_elements_calculated:
            print('Matrix elements not calculated')
            return
        
        if self.nabla_knmr is None or self.p2_knn is None:
            self.load_matrix_elements()
        nabla_knmr = np.abs(self.nabla_knmr)
        p2_knn = np.abs(self.p2_knn)
        
        E_f = self.calc.get_fermi_level()

        bz_to_ibz_map = self.calc.get_bz_to_ibz_map()

        nkpts = self.nkpts
        if bands == None:
            nbands = self.calc.get_number_of_bands()
            lb,ub = 0,nbands
        else:
            lb,ub = bands
            nbands = ub-lb
        # eps_kn = np.zeros((nkpts,nbands))
        eps_kn = np.array([self.calc.get_eigenvalues(k) for k in range(nkpts)])
        # occ_kn = np.zeros((nkpts,nbands))
        # nabla_knm_inplane = np.zeros((nkpts,nbands,nbands))
        # nabla_knm_outplane = np.zeros((nkpts,nbands,nbands))

        nabla_knm_inplane = np.linalg.norm(nabla_knmr[:,:,:,:2],axis=3)
        nabla_knm_outplane = nabla_knmr[:,:,:,2]

        # for k_idx in range(nkpts):
        #     # eps_kn[k_idx,:] = self.calc.get_eigenvalues(k_idx)
        #     nabla_knm_inplane[k_idx,:,:] = np.linalg.norm(nabla_knmr[bz_to_ibz_map==k_idx,:,:,:2],axis=3).mean(axis=0)
        #     nabla_knm_outplane[k_idx,:,:] = nabla_knmr[bz_to_ibz_map==k_idx,:,:,2].mean(axis=0)


        eps_knm = eps_kn[:,np.newaxis,:]-eps_kn[:,:,np.newaxis]
        rec_eps_knm = eps_knm.copy()
        rec_eps_knm[eps_knm==0] = 1
        rec_eps_knm = 1/rec_eps_knm
        rec_eps_knm[eps_knm==0] = 0
        rec_eps_knm = np.abs(rec_eps_knm)

        gaussian_knE = gaussian(E_E - (eps_kn[:,:, np.newaxis]-E_f), E_width) * np.exp(-E_alpha * np.abs(E_E))
        gaussian_knmE = gaussian(E_E - eps_knm[:,:,:, np.newaxis], E_width) * np.exp(-E_alpha * np.abs(E_E))
        #gaussian_knE *= np.sign(E_f - eps_kn[:,:,np.newaxis])

        # self.timer.start('np.einsum EE')
        # dos_EE = np.einsum('kme, knm, knE -> eE',gaussian_knE,np.ones(nabla_knm_inplane.shape),gaussian_knE)
        # nabla_inplane_EE = np.einsum('kme, knm, knE -> eE',gaussian_knE,nabla_knm_inplane,gaussian_knE)
        # nabla_outplane_EE = np.einsum('kme, knm, knE -> eE',gaussian_knE,nabla_knm_outplane,gaussian_knE)
        # xy_EE = np.einsum('kme, knm, knE -> eE',gaussian_knE,nabla_knm_inplane*rec_eps_knm,gaussian_knE)
        # z_EE = np.einsum('kme, knm, knE -> eE',gaussian_knE,nabla_knm_outplane*rec_eps_knm,gaussian_knE)
        # p2_EE = np.einsum('kme, knm, knE -> eE',gaussian_knE,p2_knn,gaussian_knE)
        # self.timer.stop('np.einsum EE')

        self.timer.start('np.einsum E')
        dos_E = np.einsum('knm, knE -> E',np.ones(nabla_knm_inplane.shape),gaussian_knE)
        nabla_inplane_E = np.einsum('knm, knE -> E',nabla_knm_inplane,gaussian_knE)
        nabla_outplane_E = np.einsum('knm, knE -> E',nabla_knm_outplane,gaussian_knE)
        xy_E = np.einsum('knm, knE -> E',nabla_knm_inplane*rec_eps_knm,gaussian_knE)
        z_E = np.einsum('knm, knE -> E',nabla_knm_outplane*rec_eps_knm,gaussian_knE)
        p2_E = np.einsum('knm, knE -> E',p2_knn,gaussian_knE)
        self.timer.stop('np.einsum E')
        
        triu = np.triu_indices(len(E_E))
        parprint('Done')
        self.timer.stop('Calculating ENDOME fingerprint')
        return {
                #'dos_2d_EE':dos_EE[triu],'xy_2d_EE':xy_EE[triu],'z_2d_EE':z_EE[triu],'nabla_inplane_2d_EE':nabla_inplane_EE[triu],'nabla_outplane_2d_EE':nabla_outplane_EE[triu],'p2_2d_EE':p2_EE[triu],
                'dos_1d_E':dos_E,'xy_1d_E':xy_E,'z_1d_E':z_E,'nabla_inplane_1d_E':nabla_inplane_E,'nabla_outplane_1d_E':nabla_outplane_E,'p2_1d_E':p2_E
                }
        
    def radpdos(self,
                E_E=np.linspace(-10, 10, 25),
                R_R=np.linspace(0,5, 20),
                E_width=0.5,
                R_width=0.25,
                E_alpha=0.0,
                R_alpha=0.0,
                normalize=True,
                bands=None,
                parallel=False
                ):
        self.timer.start('Calculating RAD-PDOS fingerprint')
        parprint('Calculating RAD-PDOS fingerprints')
        
        self.timer.start('Setup')
        pdos_e, pdos_ale = self.get_pdos_ale()
        
        nbands = self.calc.get_number_of_bands()
        if bands == None:
            lb,ub = 0,nbands
        else:
            lb,ub = bands
        
        K = self.nkpts
        M = 3*3*nbands*len(R_R)*len(E_E)
        start_index, end_index, local_k_size, numelements, displacements = get_domain_decomp(K, M, world.rank, world.size)
        # nkpoints = len(self.calc.get_ibz_k_points())
        kpoints = range(start_index,end_index)
        nkpoints = len(kpoints)

        a,l,e = pdos_ale.shape
        pdos_alkn = pdos_ale.reshape((a,l,self.nkpts, self.nbands))
        e_kn = pdos_e.reshape((self.nkpts, self.nbands))
        
        pdos_ale_local = pdos_alkn[:,:,kpoints,:].reshape((a,l,-1))
        pdos_e_local = e_kn[kpoints,:].flatten()
        # print('pdos_ale_local: {}'.format(pdos_ale_local.shape))
        # print('pdos_ale: {}'.format(pdos_ale.shape))
        # print('pdos_e_local: {}'.format(pdos_e_local.shape))
        # print('pdos_e: {}'.format(pdos_e.shape))

        E_f = self.calc.get_fermi_level()

        # gaussian_knE = gaussian(E_E - e_kn[:,:, np.newaxis], E_width)
        gaussian_eeE = gaussian(E_E - (pdos_e[np.newaxis,:]-pdos_e_local[:,np.newaxis])[:,:,np.newaxis], E_width)
        gaussian_eeE *= np.sign(E_f - pdos_e[np.newaxis,:,np.newaxis])
        exp_E = np.exp(-E_alpha * np.abs(E_E))
        # print('exp_E: {}'.format(exp_E.shape))
        gaussian_eeE *= exp_E
        # print('gaussian_eeE: {}'.format(gaussian_eeE.shape))

        cutoff = max(R_R) + 3 * R_width
        ijd = neighbor_list('ijd', self.calc.atoms, cutoff=cutoff,self_interaction=True)
        # print('ijd: {}'.format(len(ijd[0])))

        fp_lleRE = np.zeros((3,3,nkpoints*nbands,len(R_R),len(E_E)))
        
        #fp_knRE = np.zeros((nkpoints, nbands, len(R_R), len(E_E)))
        exp_R = np.exp(-R_alpha * R_R)
        # print('exp_R: {}'.format(exp_R.shape))
        self.timer.stop('Setup')

        self.timer.start('np.einsum')
        for i, j, d in zip(*ijd):
            x_R = gaussian(d - R_R, R_width) * exp_R

            # Matrix multiplication and summation. 'le' is angular and (n,k)-point energies for state 1 
            # and 'kf' is similar for state 2. 'efE' refers to energy distance between 'e' and 'f' and the 
            # and the corresponding weight to energy bin 'E'. 'R' is weights for radial bins. 

            for e in range(nkpoints*nbands):
                fp_lleRE[:,:,e,:,:] += np.einsum('l, kf, fE, R -> lkRE',
                                    pdos_ale_local[i,:,e],
                                    pdos_ale[j,:,:],
                                    gaussian_eeE[e,:,:],
                                    x_R,
                                    optimize=True)

        self.timer.stop('np.einsum')

        # fp_lleRE_2 = np.zeros((3,3,nkpoints*nbands,len(R_R),len(E_E)))
        # self.timer.start('np.einsum')
        # for i, j, d in zip(*ijd):
        #     x_R = gaussian(d - R_R, R_width) * exp_R

        #     # Matrix multiplication and summation. 'le' is angular and (n,k)-point energies for state 1 
        #     # and 'kf' is similar for state 2. 'efE' refers to energy distance between 'e' and 'f' and the 
        #     # and the corresponding weight to energy bin 'E'. 'R' is weights for radial bins. 

        #     fp_lleRE_2 += np.einsum('le, kf, efE, R -> lkeRE',
        #                             pdos_ale[i,:,:],
        #                             pdos_ale[j,:,:],
        #                             gaussian_eeE,
        #                             x_R,
        #                             optimize=True)
        # self.timer.stop('np.einsum')
        # assert np.all(np.isclose(fp_lleRE,fp_lleRE_2))
            
        # print('x_R: {}'.format(x_R.shape))
        
        fp_llknRE = fp_lleRE.reshape((3,3,nkpoints, nbands, len(R_R), len(E_E)))
        # parprint('PDOS fp_llknRE: {}'.format(fp_llknRE.shape))
        

        if self.calc.world.size > 1:
            self.timer.start('Gathering pdos fp from ranks')
            fp_knllRE = np.transpose(fp_llknRE,[2,3,0,1,4,5])
            MPI = MPI4PY()
            MPI_COMM_WORLD = MPI.comm
            rank = MPI_COMM_WORLD.rank
            size = MPI_COMM_WORLD.size
            local_data = fp_knllRE.reshape((local_k_size,-1)).astype(np.double) if local_k_size > 0 else np.zeros((1,M))
            global_data = np.zeros((K,M), dtype = np.double)

            MPI_COMM_WORLD.Barrier()
            MPI_COMM_WORLD.Allgatherv([local_data, local_k_size*M], [global_data, (numelements, displacements)])  
            fp_llknRE = np.transpose(global_data.reshape((self.nkpts,self.nbands,3,3,len(R_R),len(E_E))),[2,3,0,1,4,5])

            self.timer.stop('Gathering pdos fp from ranks')
        
        parprint('fp_llknRE: {}'.format(fp_llknRE.shape))
        fp_llknRE = fp_llknRE[:,:,:,lb:ub,:,:]
        parprint('fp_llknRE: {}'.format(fp_llknRE.shape))
        ll = ([0, 0, 0, 1, 1, 2], [0, 1, 2, 1, 2, 2])
        fp_llknRE = fp_llknRE[ll]
        if normalize:
            fp_llknRE = fp_llknRE/self.calc.get_number_of_electrons()

        pdos_ss_knRE = fp_llknRE[0]
        pdos_sp_knRE = fp_llknRE[1]
        pdos_sd_knRE = fp_llknRE[2]
        pdos_pp_knRE = fp_llknRE[3]
        pdos_pd_knRE = fp_llknRE[4]
        pdos_dd_knRE = fp_llknRE[5]

        parprint('Done')
        self.timer.stop('Calculating RAD-PDOS fingerprint')
        return {'pdos_ss_knRE':pdos_ss_knRE,'pdos_sp_knRE':pdos_sp_knRE,'pdos_sd_knRE':pdos_sd_knRE,'pdos_pp_knRE':pdos_pp_knRE,'pdos_pd_knRE':pdos_pd_knRE,'pdos_dd_knRE':pdos_dd_knRE}

    def radpdos_material(self,E_E=np.linspace(-10, 10, 25),
                        R_R=np.linspace(0,5, 20),
                        E_width=0.5,
                        R_width=0.25,
                        E_alpha=0.0,
                        R_alpha=0.0,
                        normalize=True,
                        bands=None):
        self.timer.start('Calculating RAD-PDOS material fingerprint')
        parprint('Calculating RAD-PDOS material fingerprints')
        
        pdos_e, pdos_ale = self.get_pdos_ale()
        
        nbands = self.calc.get_number_of_bands()
        if bands == None:
            lb,ub = 0,nbands
        else:
            lb,ub = bands
        
        K = self.nkpts
        M = 3*3*nbands*len(R_R)*len(E_E)
        start_index, end_index, local_k_size, numelements, displacements = get_domain_decomp(K, M, world.rank, world.size)
        # nkpoints = len(self.calc.get_ibz_k_points())
        kpoints = range(start_index,end_index)
        nkpoints = len(kpoints)

        E_f = self.calc.get_fermi_level()

        gaussian_eE = gaussian(E_E - (pdos_e[:, np.newaxis] - E_f), E_width)
        #gaussian_eE *= np.sign(E_f - pdos_e[:,np.newaxis])
        fp_alE = pdos_ale.dot(gaussian_eE)
        fp_alE *= np.exp(-E_alpha * np.abs(E_E))
        # parprint('gaussian_eE: {}'.format(gaussian_eE.shape))
        # parprint('pdos_ale: {}'.format(pdos_ale.shape))
        # parprint('fp_alE: {}'.format(fp_alE.shape))

        cutoff = max(R_R) + 3 * R_width
        ijd = neighbor_list('ijd', self.calc.atoms, cutoff=cutoff,self_interaction=True)
        # parprint('ijd: {}'.format(len(ijd[0])))

        fp2_llRE = np.zeros((3, 3, len(R_R), len(E_E)))
        fp_llRe = np.zeros((3, 3, len(R_R), len(pdos_e)))
        exp_R = np.exp(-R_alpha * R_R)
        for i, j, d in zip(*ijd):
            x_R = gaussian(d - R_R, R_width) * exp_R
            fp2_llRE += np.einsum('lE, kE, R -> lkRE',
                                fp_alE[i, :],
                                fp_alE[j, :],
                                x_R,
                                optimize=True)
            fp_llRe += np.einsum('le, ke, R -> lkRe',
                                pdos_ale[i, :],
                                pdos_ale[j, :],
                                x_R,
                                optimize=True)
        # parprint('x_R: {}'.format(x_R.shape))
        fp1_llRE = fp_llRe.dot(gaussian_eE)
        fp1_llRE *= np.exp(-E_alpha * np.abs(E_E))

        if normalize:
            fp1_llRE = fp1_llRE/self.calc.get_number_of_electrons()
            fp2_llRE = fp2_llRE/self.calc.get_number_of_electrons()


        ll = ([0, 0, 0, 1, 1, 2], [0, 1, 2, 1, 2, 2])
        fp1_llRE = fp1_llRE[ll]
        fp2_llRE = fp2_llRE[ll]

        pdos1_ss_RE = fp1_llRE[0]
        pdos1_sp_RE = fp1_llRE[1]
        pdos1_sd_RE = fp1_llRE[2]
        pdos1_pp_RE = fp1_llRE[3]
        pdos1_pd_RE = fp1_llRE[4]
        pdos1_dd_RE = fp1_llRE[5]
        
        pdos2_ss_RE = fp2_llRE[0]
        pdos2_sp_RE = fp2_llRE[1]
        pdos2_sd_RE = fp2_llRE[2]
        pdos2_pp_RE = fp2_llRE[3]
        pdos2_pd_RE = fp2_llRE[4]
        pdos2_dd_RE = fp2_llRE[5]

        parprint('Done')
        self.timer.stop('Calculating RAD-PDOS material fingerprint')
        return {'pdos1_ss_RE':pdos1_ss_RE,'pdos1_sp_RE':pdos1_sp_RE,'pdos1_sd_RE':pdos1_sd_RE,'pdos1_pp_RE':pdos1_pp_RE,'pdos1_pd_RE':pdos1_pd_RE,'pdos1_dd_RE':pdos1_dd_RE,
                'pdos2_ss_RE':pdos2_ss_RE,'pdos2_sp_RE':pdos2_sp_RE,'pdos2_sd_RE':pdos2_sd_RE,'pdos2_pp_RE':pdos2_pp_RE,'pdos2_pd_RE':pdos2_pd_RE,'pdos2_dd_RE':pdos2_dd_RE
                }


    def get_pdos_ale(self):
        self.timer.start('Calculating pdos_ale')
        n_atoms = len(self.calc.atoms)
        n_kpts = len(self.calc.get_ibz_k_points())
        n_bands = self.calc.get_number_of_bands()
        # print('n_kpts: {}'.format(n_kpts))
        # print('n_bands: {}'.format(n_bands))
        # Get PDOS data for specific atom and orbital
        angulars = ['s', 'p', 'd']
        pdos_ale = np.zeros((n_atoms, len(angulars), n_bands * n_kpts))
        pdos_e = np.zeros(n_bands * n_kpts)
        for atom_idx in range(n_atoms):
            for i_angular, angular in enumerate(angulars):
                e, w = raw_orbital_LDOS(self.calc,
                                        a=atom_idx,
                                        spin=0,
                                        angular=angular)
                pdos_ale[atom_idx][i_angular] = w

        pdos_e = e * Ha
        self.timer.stop('Calculating pdos_ale')
        return pdos_e, pdos_ale

    def get_fingerprint(self,
                        matrix_elements={'E_min':-10,'E_max':10,'n_E':50,'E_width':0.3,'E_alpha':1/5},
                        pdos={'E_min':-10,'E_max':10,'n_E':25,'E_width':0.3,'E_alpha':1/5,'R_min':0,'R_max':5,'n_R':20,'R_width':0.25,'R_alpha':1/3},
                        fp_mode='state',
                        bands=None,
                        kpts=None,
                        return_as_df = True,
                        gw_data=False
                        ):
        
        if gw_data:
            lb,ub = get_bandrange(self.calc)
            ub = min(ub,self.nbands)
            nbands = ub-lb
        elif bands == None:
            nbands = self.nbands
            lb,ub = 0,nbands
        else:
            lb,ub = bands
            ub = min(ub,self.nbands)
            nbands = ub-lb
        bands=(lb,ub)

        fp_dict = {}
        if fp_mode == 'state':
            if matrix_elements is not None:
                fp_dict.update(self.endome(E_E=np.linspace(matrix_elements['E_min'],matrix_elements['E_max'],matrix_elements['n_E']),E_width=matrix_elements['E_width'],E_alpha=matrix_elements['E_alpha'],bands=bands))
            if pdos is not None:
                fp_dict.update(self.radpdos(E_E=np.linspace(pdos['E_min'],pdos['E_max'],pdos['n_E']),E_width=pdos['E_width'],E_alpha=pdos['E_alpha'],R_R=np.linspace(pdos['R_min'],pdos['R_max'],pdos['n_R']),R_width=pdos['R_width'],R_alpha=pdos['R_alpha'],bands=bands))        
        elif fp_mode == 'material':
            if matrix_elements is not None:
                fp_dict.update(self.endome_material(E_E=np.linspace(matrix_elements['E_min'],matrix_elements['E_max'],matrix_elements['n_E']),E_width=matrix_elements['E_width'],E_alpha=matrix_elements['E_alpha'],bands=bands))
            if pdos is not None:
                fp_dict.update(self.radpdos_material(E_E=np.linspace(pdos['E_min'],pdos['E_max'],pdos['n_E']),E_width=pdos['E_width'],E_alpha=pdos['E_alpha'],R_R=np.linspace(pdos['R_min'],pdos['R_max'],pdos['n_R']),R_width=pdos['R_width'],R_alpha=pdos['R_alpha'],bands=bands))        
        

        if not return_as_df:
            return fp_dict
        else:
            self.timer.start('Returning dataframe')
            nkpts = len(self.calc.get_ibz_k_points())

            #print('nkpts*nbands = {}'.format(nkpts*nbands))
            df = pd.DataFrame(index=range(1 if fp_mode=='material' else nkpts*nbands))
            for key in fp_dict.keys():
                #print(key,fp_dict[key].shape)
                fp = fp_dict[key].reshape((1,-1) if fp_mode=='material' else (nkpts*nbands,-1))
                df.loc[:,[key+str(i) for i in range(fp.shape[-1])]] = fp
            if fp_mode == 'state':
                N,K = np.meshgrid(range(lb,ub),range(nkpts))
                eps_kn = np.array([self.calc.get_eigenvalues(i)[lb:ub] for i in range(nkpts)])
                occ_kn = np.array([self.calc.get_occupation_numbers(i)[lb:ub] for i in range(nkpts)])
                E_f = self.calc.get_fermi_level()
                delta_Ef_kn = eps_kn - E_f
                df.loc[:,'n_idx'] = N.reshape(nkpts*nbands,1)
                df.loc[:,'k_idx'] = K.reshape(nkpts*nbands,1)
                df.loc[:,'eps'] = eps_kn.reshape(nkpts*nbands,1)
                df.loc[:,'occ'] = occ_kn.reshape(nkpts*nbands,1)
                df.loc[:,'delta_Ef'] = delta_Ef_kn.reshape(nkpts*nbands,1)
                df.loc[:,'cbm'] = False
                df.loc[:,'vbm'] = False
                for k_idx in df.k_idx.unique():
                    vbm = df.loc[(df.k_idx==k_idx)&(df.delta_Ef<0),'delta_Ef'].idxmax()
                    cbm = df.loc[(df.k_idx==k_idx)&(df.delta_Ef>0),'delta_Ef'].idxmin()
                    df.loc[cbm,'cbm'] = True
                    df.loc[vbm,'vbm'] = True
                
                if gw_data:
                    gw_res = read_json(self.path+'/results-asr.gw@gw.json')
                    df = df[(df.n_idx>=lb)&(df.n_idx<ub)]
                    qp = gw_res['qp'][:,:,:nbands]
                    nspins,nkpts,nbands = qp.shape
                    parprint('GW data: nspins = {},nkpts = {},nbands = {}'.format(nspins,nkpts,nbands))
                    Z = gw_res['Z'][:,:,:nbands]
                    df.loc[:,'Z'] = Z.reshape(nkpts*nbands,1)
                    df.loc[:,'qp'] = qp.reshape(nkpts*nbands,1)

            self.timer.stop('Returning dataframe')
            self.fp_df = df
            return df
    
    def get_predictions(self,interpolate=False,fp_distribution=False,model_path=None):
        self.timer.start('Getting predictions')
        if self.fp_df is None:
            df = self.get_fingerprint()
        else:
            df = self.fp_df

        if model_path is None:
            model_path = self.model_path

        if os.path.isfile(self.path+'results-asr.polarizability.json'):
            state_dict = torch.load('nn_model.state_dict')
            net = Net(state_dict['num_features'],state_dict['num_l1'],state_dict['num_l2'],state_dict['num_l3'],state_dict['num_output'])
            net.load_state_dict(state_dict['model_state_dict'])
            features = state_dict['features']
            net.eval()
            if 'alphaxy_el' in features:
                res = read_json(self.path+'results-asr.polarizability.json')
                alphax_el = res['alphax_el']
                alphay_el = res['alphay_el']
                df.loc[:,'alphaxy_el'] = 0.5*(alphax_el+alphay_el)
            if 'alphaz_el' in features:
                df.loc[:,'alphaz_el'] = res['alphaz_el']
        else:
            state_dict = torch.load('nn_model_noalphas.state_dict')
            net = Net(state_dict['num_features'],state_dict['num_l1'],state_dict['num_l2'],state_dict['num_l3'],state_dict['num_output'])
            net.load_state_dict(state_dict['model_state_dict'])
            features = state_dict['features']
            net.eval()
            if 'alphaxy_el' in features:
                alphax_el = 0
                alphay_el = 0
                df.loc[:,'alphaxy_el'] = 0.5*(alphax_el+alphay_el)
            if 'alphaz_el' in features:
                df.loc[:,'alphaz_el'] = 0
        
        if 'gap' in features:
            df.loc[:,'gap'] = read_json(self.path+'results-asr.gs.json')['gap']

        # model = xgb.Booster()
        # model.load_model(model_path)
        # features = model.feature_names
        # fp_features = [feature for feature in features if any([f.isdigit() for f in feature])]
        # other_features = [feature for feature in features if feature not in fp_features]


        # self.timer.start('Predicting')
        # df['pred'] = model.predict(xgb.DMatrix(data=df[features]))
        # self.timer.stop('Predicting')

        self.timer.start('Predicting')
        with torch.no_grad():
            df['pred'] = net(torch.from_numpy(df[features].values).float()).numpy()
        self.timer.stop('Predicting')

        
        if fp_distribution:
            N_samples = 1000
            df['pred_single'] = df['pred']
            stds = df[fp_features].std()
            # df_temp = pd.DataFrame(index=range(len(df)*10),columns=df.columns)
            # print(df_temp.shape)
            # for i,idx in enumerate(df.index):
            #     print(idx)
            #     df_temp.loc[i*10:(i+1)*10-1,fp_features] = np.random.normal(df.loc[idx,fp_features].values.astype(np.float64),0.05*stds.values,(10,len(fp_features)))
            #     df_temp.loc[i*10:(i+1)*10-1,[f for f in df.columns if f not in fp_features]] = df.loc[idx,[f for f in df.columns if f not in fp_features]].values
                        
            # df_temp['pred'] = model.predict(xgb.DMatrix(data=df_temp[features]))
            # print(df_temp)

            for n in [5]:#df.n_idx.unique():
                for k in df.k_idx.unique():
                    print(n,k)
                    df_temp = pd.DataFrame(data=np.random.normal(df.loc[(df.n_idx==n)&(df.k_idx==k),fp_features].values,stds.values*0.05,(N_samples,len(fp_features))),
                                           columns=fp_features)
                    # df_temp.loc[:,other_features] = np.random.normal(df.loc[(df.n_idx==n)&(df.k_idx==k),other_features].values,np.abs(0.05*df.loc[(df.n_idx==n)&(df.k_idx==k),other_features].values),(1000,len(other_features)))
                    df_temp.loc[:,other_features] = df.loc[(df.n_idx==n)&(df.k_idx==k),other_features].values
                    pred = model.predict(xgb.DMatrix(data=df_temp))
                    df.loc[(df.n_idx==n)&(df.k_idx==k),'pred'] = np.mean(pred)
                    
        if interpolate:
            from scipy.ndimage import gaussian_filter1d            
            for n in df.n_idx.unique():
                df.loc[df.n_idx==n,'pred'] = gaussian_filter1d(df.loc[df.n_idx==n,'pred'],sigma=5)
        


        df['pred_qp'] = df['eps'] + df['pred']  
        self.timer.stop('Getting predictions')
        return df
    
    def update_eigenvalues(self,interpolate=False):
        if not self.eigenvalues_updated:
            self.timer.start('Updating eigenvalues')
            df = self.get_predictions(interpolate=interpolate)
            nkpts = len(self.calc.get_ibz_k_points())
            nbands = self.calc.get_number_of_bands()
            pred_qp = df.pred_qp.values.reshape(nkpts,nbands)
            for q,kpt in enumerate(self.calc.wfs.kpt_u):
                self.calc.wfs.kpt_u[q].eps_n = pred_qp[kpt.k]/Ha
            self.calc.write(self.calc_name.split('.gpw')[0] + '_ml.gpw')
            self.eigenvalues_updated = True
            self.timer.stop('Updating eigenvalues')
        else:
            parprint('Eigenvalues already updated')
    
    def plot_bandstructures(self,filename=None,interpolate=False):
        parprint('Plotting band structures')
        self.timer.start('Plotting band structures')
        calc = GPAW(self.calc_name)
        bs = calc.band_structure()

        if not self.eigenvalues_updated:
            self.update_eigenvalues(interpolate=interpolate)
        bs_ml = self.calc.band_structure()

        fig,ax = plt.subplots(1,1)
        bs.plot(ax=ax,spin=0,colors=['C1'],ylabel=r'$E-E_F$ [eV]',label='PBE',**{'linewidth':0.8})
        bs_ml.plot(ax=ax,spin=0,colors=['C2'],ylabel=r'$E-E_F$ [eV]',label='ML',**{'linewidth':0.8})
        ax.legend(loc='upper right')

        if filename == None:
            filename = self.calc_name.split('.gpw')[0]+'_ml.png'
        fig.savefig(filename)
        self.timer.stop('Plotting band structures')

def collect_dataset(tree_path= './',calc_name='gs_gw_nowfs.gpw',fp_mode='state',
                    gw_data=True,filter_Z=False,filter_vc=False,filter_k=False,
                    matrix_elements={'E_min':-10,'E_max':10,'n_E':50,'E_width':0.3,'E_alpha':1/5},
                    pdos={'E_min':-10,'E_max':10,'n_E':25,'E_width':0.3,'E_alpha':1/5,'R_min':0,'R_max':5,'n_R':20,'R_width':0.25,'R_alpha':1/3}
                    ):
    if not tree_path[-1] == '/':
        tree_path += '/'
    print(tree_path)

    count = 0
    df = pd.DataFrame()
    for f1 in [f for f in os.listdir(tree_path) if os.path.isdir(tree_path+f)]:
        for f2 in os.listdir(tree_path+f1):
            for material in os.listdir(tree_path+f1+'/'+f2):
                material_path = tree_path+f1+'/'+f2+'/'+material+'/'
                if fp_mode == 'material':
                    include_material = os.path.isfile(material_path+calc_name) and os.path.isfile(material_path+'gs_matrix_elements.npz')
                elif fp_mode == 'state':
                    include_material = os.path.isfile(material_path+calc_name) and os.path.isfile(material_path+'results-asr.gw@gw.json') if gw_data else True
                
                if include_material:
                    count += 1
                    parprint('{} {}'.format(count,material))
                    ML = MLGPAW(material_path+calc_name)

                    df_material = ML.get_fingerprint(matrix_elements=matrix_elements,pdos=pdos,gw_data=gw_data,fp_mode=fp_mode)
                    df_material.loc[:,'material'] = material
                    query_idx = df_material.material.notna()
                    
                    if fp_mode == 'state':
                        if gw_data and filter_Z:
                            query_idx = query_idx & (df_material.Z >= filter_Z[0]) & (df_material.Z <= filter_Z[1])

                        if filter_vc:
                            if 'vb' in filter_vc and 'cb' in filter_vc:
                                query_idx = query_idx & (df_material.cbm | df_material.vbm)
                            elif 'vb' in filter_vc:
                                query_idx = query_idx & (df_material.vbm)
                            elif 'cb' in filter_vc:
                                query_idx = query_idx & (df_material.cbm)
                        if filter_k:
                            query_idx = query_idx & df_material.k_idx.map(lambda x: x%filter_k==0)

                    df_material = df_material.loc[query_idx]
                    parprint('df_material: {}'.format(df_material.shape))

                    df = df.append(df_material,ignore_index=True)
                    parprint('df: {}'.format(df.shape))
                    parprint(df.tail())
    return df

def gaussian(x,sigma=1,mu=0):
    return 1 / (sigma * np.sqrt(2 * np.pi)) * np.exp(-0.5 * ((x-mu) / sigma)**2)

def get_bandrange(calc):
		# bands (-8,4)
		lb, ub = max(calc.wfs.nvalence // 2 - 8, 0), calc.wfs.nvalence // 2 + 4
		return lb, ub

def plot_band_contour(calc_name,n,fig=None,ax=None,savefig=True):
    from ase.dft.bandgap import bandgap
    from asr.utils.gpw2eigs import calc2eigs

    calc = GPAW(calc_name)
    e_skn, efermi = calc2eigs(calc, soc=False)
    gap, (s1, k1, n1), (s2, k2, n2) = bandgap(eigenvalues=e_skn, efermi=efermi,
                                              output='-')
    if n == 'vb':
        n_idx = n1
    elif n == 'cb':
        n_idx = n2
    else:
        n_idx = n

    kpts = calc.get_bz_k_points()
    bzmap = calc.get_bz_to_ibz_map()
    x = kpts[:,0]
    y = kpts[:,1]
    z = [calc.get_eigenvalues(bzmap[k])[n_idx] for k in range(len(kpts))]

    if fig is None and ax is None:
        fig,ax = plt.subplots()
    ax.tricontour(x, y, z, levels=14, linewidths=0.5, colors='k')
    cntr = ax.tricontourf(x, y, z, levels=14, cmap="RdBu_r")
    fig.colorbar(cntr, ax=ax)
    ax.plot(x, y, 'ko', ms=3)
    if savefig:
        fig.savefig('contour_{}_n={}.png'.format(calc_name.split('.gpw')[0],n))

if __name__ == '__main__':
    df = collect_dataset(tree_path= './tree/',calc_name='gs.gpw',fp_mode='material',pdos=None)
    df.to_json('df_material.json')

import numpy as np
import sys
import os
import time
from ase.parallel import MPI4PY, world

def get_domain_decomp(N, M, rank, size):
    """
    Function to calculate a 1D domain decomposition of a N-element array among a processor pool of "size" processors
    
    Input:
        * N:                Array size
        * rank:             Rank of this processor
        * size:             Size of processor poll
    Output: 
        * start_index:      First index that this processor should take
        * end_index:        Last index that this processor should take
        * local_k_size:     Local array size
        * numelements:      Number of elements on the different processors
        * displacements:    Displacements corrosponding to the different processors
    """


        
    res = N%size
    if(rank < size - res):
        local_k_size = int(N/size)
    else:
        local_k_size = int(N/size) + 1
    # Adding 1 row to the processors that have ranks less that the residue
    ranks = np.linspace(0, size-1, size, dtype = "int") 
    numelements = np.zeros(size)
    numelements[ranks >= size - res] = (int(N/size) + 1)*M
    numelements[ranks < size - res] = int(N/size)*M
    # Next we need the displacements
    displacements = np.linspace(0, size-1, size, dtype = "int")
    displacements[ranks < size - res] = M*(int(N/size))*displacements[ranks < size - res]
    displacements[ranks >= size - res] = M*(int(N/size)+1)*(displacements[ranks >= size - res] - (size-res)) + (size-res)*(int(N/size))*M


    #displacements[ranks < res] = M*(int(N/size) + 1)*displacements[ranks < res]
    #displacements[ranks >= res] = M*(int(N/size))*(displacements[ranks >= res] - res) + res*(int(N/size) + 1)*M
    ## Converting to tuple
    numelements = tuple(numelements)
    displacements = tuple(displacements)


    ## Making the local ks-array
    if(rank < size - res):
        start_index = int(N/size)*rank
        end_index = int(N/size)*(rank + 1)
    else:
        start_index = int(N/size+1)*(rank - (size-res)) + (size-res)*int(N/size )
        end_index = int(N/size+1)*(rank + 1 - (size-res)) + (size-res)*int(N/size)
    
    return start_index, end_index, local_k_size, numelements, displacements



if __name__ == '__main__':
    calc = GPAW('/home/niflheim/nirkn/electronic-structure-machine-learning/tree/A/As/As2-2cfe371476fd/wfs_test.gpw')
    p2_knn = p2('/home/niflheim/nirkn/electronic-structure-machine-learning/tree/A/As/As2-2cfe371476fd/wfs_test.gpw')

    print('Rank {} has {} k-points. p2 shape = {}'.format(world.rank,len(calc.wfs.kpt_qs),p2_knn.shape))
    K = len(calc.get_ibz_k_points())
    N = calc.get_number_of_bands()
    M = N*N

    MPI = MPI4PY()
    MPI_COMM_WORLD = MPI.comm
    rank = MPI_COMM_WORLD.rank
    size = MPI_COMM_WORLD.size
    start_index, end_index, local_k_size, numelements, displacements = get_domain_decomp(K, M, rank, size)

    print("My rank is {} and I need to take {} elements of k and I have the following start {} and end {} index".format(rank, local_k_size, start_index, end_index))

    local_data = p2_knn.reshape((local_k_size,N*N))
    global_data = np.zeros((K,N*N), dtype = 'complex128')


    MPI_COMM_WORLD.Barrier()
    MPI_COMM_WORLD.Allgatherv([local_data, local_k_size*N*N], [global_data, (numelements, displacements)])  

    global_data = global_data.reshape((K,N,N))
    np.save('/home/niflheim/nirkn/electronic-structure-machine-learning/tree/A/As/As2-2cfe371476fd/p2_knn_parallel{}.npy'.format(world.size),global_data)
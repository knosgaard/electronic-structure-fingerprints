# Electronic structure fingerprints

System requirements:
- [gpaw](https://wiki.fysik.dtu.dk/gpaw/) 21.1.1
- [ase](https://wiki.fysik.dtu.dk/ase/) 3.22
- [numpy](https://numpy.org/) 1.19.4
- [pandas](https://pandas.pydata.org/) 1.1.4
- [xgboost](https://xgboost.readthedocs.io/en/stable/) 1.1.1
- [pytorch](https://pytorch.org/)

## Example
The code requires a GPAW object. For this example a ground state calculation of MoS2 is used.

Load gpaw file. This will check if matrix elements are calculated. If not, the matrix elements are calculated using GPAW

`ML = MLGPAW("mos2.gpw")`

Calculate ENDOME fingerprint:

`endome = ML.endome(E_E=np.linspace(-10, 10, 25), E_width=0.5, E_alpha=0.0)`

Calculate RAD-PDOS fingerprint:

`radpdos = ML.radpdos(E_E=np.linspace(-10, 10, 25), R_R=np.linspace(0,5, 20), E_width=0.5, R_width=0.25, E_alpha=0.0, R_alpha=0.0`

These functions will return dictionaries of numpy arrays corresponding to the different fingerprints.

import torch
import torch.nn as nn

class Net(nn.Module):
    def __init__(self,num_features,num_l1,num_l2,num_l3,num_output=1,activation='tanh'):
        super(Net, self).__init__()
                
        self.dropout = nn.Dropout(p=0.1)
        self.l1 = nn.Linear(num_features,num_l1)
        self.l2 = nn.Linear(num_l1,num_l2)
        self.l3 = nn.Linear(num_l2,num_l3)
        self.L = nn.Linear(num_l3,num_output)
        
        if activation=='relu':
            self.activation = nn.ReLU()
            self.init_weights = nn.init.kaiming_normal_

            self.init_weights(self.l1.weight,nonlinearity='relu')
            self.init_weights(self.l2.weight,nonlinearity='relu')
            self.init_weights(self.l3.weight,nonlinearity='relu')
            self.init_weights(self.L.weight,nonlinearity='relu')
        elif activation=='tanh':
            self.activation = nn.Tanh()
            self.init_weights = nn.init.xavier_normal_

            self.init_weights(self.l1.weight,nn.init.calculate_gain('tanh'))
            self.init_weights(self.l2.weight,nn.init.calculate_gain('tanh'))
            self.init_weights(self.l3.weight,nn.init.calculate_gain('tanh'))
            self.init_weights(self.L.weight,nn.init.calculate_gain('tanh'))

        
        
    # x represents our data
    def forward(self, x):
        #x = self.batchnorm(x)
        x = self.l1(x)
        x = self.activation(x)
        x = self.dropout(x)

        x = self.l2(x)
        x = self.activation(x)
        x = self.dropout(x)

        x = self.l3(x)
        x = self.activation(x)
        x = self.dropout(x)

        x = self.L(x)
        return x